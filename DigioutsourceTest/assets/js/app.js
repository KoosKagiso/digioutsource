var carousel = document.querySelector("[data-target='carousel']");
var card = carousel.querySelector("[data-target='card']");
var leftButton = document.querySelector("[data-action='slideLeft']");
var rightButton = document.querySelector("[data-action='slideRight']");

var carouselWidth = carousel.offsetWidth;
var cardStyle = card.currentStyle || window.getComputedStyle(card)
var cardMarginRight = Number(cardStyle.marginRight.match(/\d+/g)[0]);


var cardCount = carousel.querySelectorAll("[data-target='card']").length;
var offset = 0;
var maxX = -((cardCount / 3) * carouselWidth + 
               (cardMarginRight * (cardCount / 3)) - 
               carouselWidth - cardMarginRight);

leftButton.addEventListener("click", function() {
  if (offset !== 0) {
    offset += carouselWidth + cardMarginRight;
    // carousel.style.transform = `translateX(${offset}px)`;
    carousel.style.transform = "translateX("+offset+"px)";
    }
})
  
rightButton.addEventListener("click", function() {
  if (offset !== maxX) {
    offset -= carouselWidth + cardMarginRight;
   carousel.style.transform = "translateX("+offset+"px)";
  }
})