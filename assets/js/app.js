var carousel = document.querySelector("[data-target='carousel']");
var card = carousel.querySelector("[data-target='card']");
var leftButton = document.querySelector("[data-action='slideLeft']");
var rightButton = document.querySelector("[data-action='slideRight']");
var carouselWidth = carousel.offsetWidth;
var cardStyle = card.currentStyle || window.getComputedStyle(card)
var cardMarginRight = Number(cardStyle.marginRight.match(/\d+/g)[0]);
var cardCount = carousel.querySelectorAll("[data-target='card']").length;
var offset = 0;
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0]; 
var maxX = -((cardCount / 3) * carouselWidth + 
               (cardMarginRight * (cardCount / 3)) - 
               carouselWidth - cardMarginRight);

leftButton.addEventListener("click", function() {
  if (offset !== 0) {
    offset += carouselWidth + cardMarginRight;
    // carousel.style.transform = `translateX(${offset}px)`;
    carousel.style.transform = "translateX("+offset+"px)";
    }
})
  
rightButton.addEventListener("click", function() {
  if (offset !== maxX) {
    offset -= carouselWidth + cardMarginRight;
   carousel.style.transform = "translateX("+offset+"px)";
  }
})

btn.onclick = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
/** Scroll to top button implementation in vanilla JavaScript (ES6 - ECMAScript 6) **/

var intervalId = 0; // Needed to cancel the scrolling when we're at the top of the page
var $scrollButton = document.querySelector('.scroll'); // Reference to our scroll button

function scrollStep() {
    // Check if we're at the top already. If so, stop scrolling by clearing the interval
    if (window.pageYOffset === 0) {
        clearInterval(intervalId);
    }
    window.scroll(0, window.pageYOffset - 50);
}

function scrollToTop() {
    // Call the function scrollStep() every 16.66 millisecons
    intervalId = setInterval(scrollStep, 16.66);
}

// When the DOM is loaded, this click handler is added to our scroll button
$scrollButton.addEventListener('click', scrollToTop);

var button = document.getElementById('hamburger-menu'),
    span = button.getElementsByTagName('span')[0];

button.onclick =  function() {
  span.classList.toggle('hamburger-menu-button-close');
};

$('#hamburger-menu').on('click', toggleOnClass);

function toggleOnClass(event) {
  var toggleElementId = '#' + $(this).data('toggle'),
  element = $(toggleElementId);

  element.toggleClass('on');

}

// close hamburger menu after click a
$( '.menu li a' ).on("click", function(){
  $('#hamburger-menu').click();
});

